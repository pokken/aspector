'''
Created on Jan 20, 2014

@author: rpsands
@note controller for report model
'''

from ferris import Controller, scaffold, route, route_with, add_authorizations, auth, messages, model_form
from google.appengine.ext import ndb
from app.models.tag import Tag
from app.models.aspect import Aspect
from app.models.report import Report
from google.appengine.api import users
from google.appengine.api.logservice import logging
# import my util class for decoding a list of urlsafe keys into a list of keys
from app import util
import wtforms, cgi


class Reports(Controller):
    class Meta:
        prefixes = ('admin',)
        components = (scaffold.Scaffolding, messages.Messaging)

    """ Custom report model form for submitting a report """
    class ReportModelForm(model_form(Report, field_args={"description": {"label": "Describe the issue"},
                                                         "reported": {"widget": wtforms.widgets.HiddenInput() },
                                                         })):
        """ """
        #description = wtforms.StringField("Describe the Issue")

    # Admin handlers
    admin_list = scaffold.list
    admin_view = scaffold.view        #view a post
    admin_add = scaffold.add          #add a new post
    admin_edit = scaffold.edit        #edit a post
    admin_delete = scaffold.delete    #delete a post
    
    # Standard handlers
    # list = scaffold.list
    #add = scaffold.add
    # edit = scaffold.edit
    
    """ The Report handler accepts a key of an object to report then does some crap with it then files the report
    """
    @add_authorizations(auth.require_user)
    @route_with(template='/reports/<key>/report')
    def report(self, key):
        # make the form
        form = self.ReportModelForm()
        # parse the reported stuff
        reported_key = self.util.decode_key(key)
        # fetch the associated aspect so we have it to work with
        reported = reported_key.get()
        # set some context variables to make the form work
        self.context['reported_key'] = reported_key
        self.context['reported_name'] = reported.name
        self.context['form'] = form
        # parse parse
        self.parse_request(container=form)
        # assign the form
        self.scaffold.ModelForm = form
        
        # sets the redirect to view instead of normal list
        def set_redirect(controller, container, item):
            controller.scaffold.redirect = controller.uri('tags:browse')
        self.events.scaffold_after_save += set_redirect
        
        if self.request.method != 'GET' and form.validate():
            return scaffold.add(self)
    
    """ function to store likes on any key object that supports it """
    @add_authorizations(auth.require_user)
    @route_with(template='/reports/<key>/like')
    def like(self, key):
        # set the view to json
        self.meta.change_view('json')
        
    # Do the work
        # get the user
        user = users.get_current_user()
    
        # decode the key -- this is what we're liking    
        like_key = self.util.decode_key(key)
        like_target = like_key.get()
        # add the like
        response = like_target.toggleLike(user.user_id())
        
        
        
        self.context['data'] = response
        
        
        
        