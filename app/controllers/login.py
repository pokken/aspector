'''
Created on Jan 15, 2014

@author: rpsands
'''
from ferris import Controller, scaffold, route, add_authorizations, auth
from ferris.components import oauth
from apiclient.discovery import build

class Login(Controller):
    class Meta:
        components = (oauth.OAuth,)
        oauth_scopes = ['https://www.googleapis.com/auth/userinfo.profile']
        
    @oauth.require_credentials
    def list(self):
        # Signed HTTP instance.
        http = self.oauth.http()

        # Access to Google's OAuth info API
        service = build('oauth2', 'v2', http=http)

        user_info = service.userinfo().get().execute()

        return "Hello, you are: %s" % user_info['name']