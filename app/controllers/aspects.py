'''
Created on Jan 14, 2014

@author: rpsands
'''
from ferris import Controller, scaffold, route, add_authorizations, auth, messages
from ferris import model_form
from google.appengine.ext import ndb
from app.models.aspect import Aspect
from app.models.tag import Tag
from app.controllers.profiles import Profiles
# we import this as aspector_auth so we can leverage it as well as ferris.core.auth above
from app import auth as aspector_auth
import cgi, wtforms, re
from google.appengine.api.logservice import logging
from ferris.components.search import Search
from ferris.core import search
from app.forms.widgets import TagsCheckboxWidget
from google.appengine.api import users
#from app.forms.fields import TagMultipleReferenceField
#from ferris.components import oauth
#from apiclient.discovery import build


""" Validation functions for aspect name """
def validate_noduplicate(form, field):
    if ( Aspect.is_duplicate(field._value() )):
        raise ValueError('"' + field._value() + '"' + " already exists in the data store.")

# Message support classes
class AspectMsg(messages.Message):
    name = messages.StringField(1)
    score = messages.IntegerField(2)
    tags = messages.StringField(3, repeated=True)
    # added to support the browse fetching page
    key = messages.StringField(4)
    liked = messages.BooleanField(5)

class AspectMsgs(messages.Message):
    msg = messages.MessageField(AspectMsg, 1, repeated=True)
    

class Aspects(Controller):
    
    class Meta:
        prefixes = ('admin', 'api',)
        components = (scaffold.Scaffolding, Search, messages.Messaging, )
        
        
    """ Create the add model form -- note how we specify a query and custom widget for tags in field_args
        Note how the validator is specified for the name field!
    """
    class AspectAddForm(model_form(Aspect, exclude=('score','likes',), 
                                   field_args={"name": {"validators": [validate_noduplicate]},
                                               "tags": {"widget": TagsCheckboxWidget(),
                                                        "query": Tag.query().order(Tag.name),
                                                        
                                                         }
                                               }
                                    )
                        ):
        """Basic form for aspects! Excludes the score since it's always 0"""
        # adding the name as a wtforms textfield and assigning a validator
        #name = wtforms.TextField("Name", validators=[validate_noduplicate])
        
    
    """ Create the add model form -- note how we specify a query and custom widget for tags in field_args"""
    class AspectEditForm(model_form(Aspect, exclude=('score','name','likes',), 
                                   field_args={"tags": {"widget": TagsCheckboxWidget(),
                                                        "query": Tag.query().order(Tag.name) } } )):
        """Basic edit form for Aspects! Note it excludes the name. You should never change an aspects name
        for obvious reasons. """
      
    # admin controllers
    admin_list = scaffold.list # list all aspects
    admin_view = scaffold.view        #view a post
    admin_add = scaffold.add          #add a new post
    admin_edit = scaffold.edit        #edit a post
    admin_delete = scaffold.delete    #delete a post
    
    

    
    """ API controllers """
    #api_list = scaffold.list
    
         
    """ Fetches all the aspects that match a particular tag key  """
    @route
    def api_tag(self, key):
        # self.meta.change_view('json')
        tag_key = self.util.decode_key(key)
        tag_obj = tag_key.get()
        aspects = Aspect.all_aspects_by_tag(tag_key)
        # get the current user since we need it
        user = users.get_current_user()
        # We pass the aspects we searched for using the tag key back as the aspects context
        
        ## Serialization section
        # make the output object
        aspect_msgs = AspectMsgs()
        for aspect in aspects:
            aspect_msg = AspectMsg()
            # assign name
            aspect_msg.name = aspect.name
            # assign score
            aspect_msg.score = aspect.score
            # assign key
            aspect_msg.key = aspect.key.urlsafe()
            # more complex because of the need to create all the tag objects
            for tag in aspect.tags:
                aspect_msg.tags.append(tag.urlsafe())
            # add whether the aspect is liked - only if the user is logged in
            if ( user ):
                aspect_msg.liked = aspect.is_liked(user_id=user.user_id())
            
            # add the tag_msgs to the message
            aspect_msgs.msg.append(aspect_msg)
            
        ## end serialization
          

        # set the data to our messages so json returns
        self.context['data'] = aspect_msgs
        # This stuff is just if we need to display this stuff in a page
        # self.context['aspects'] = aspects
        # then we pass the tag back as the tag object
        # self.context['tag'] = tag_obj
     
    """Api function that returns all of the aspects that match both; expects a list of json keys in the body
        as {"keys":[list]}
    """
    @route
    def api_tags(self):
            # get a logger
        logging.getLogger().setLevel(logging.INFO)
        # get the current user since we need it
        user = users.get_current_user()
        # reject non-post requests
        if self.request.method != "POST":
            return "No Non-Post requests"

        # Do the querying
        
        # let's parse the response into json
        jsonp = self.util.parse_json(self.request.body)
        logging.debug(jsonp['keys'])
        # get all the key objects from the datastore based on our list of urlsafe keys
        keys = [ndb.Key(urlsafe=key) for key in jsonp['keys']]
        # now that we've got our keys, query for a list of aspects in the first one
        # to save time later.
        logging.debug("initial search is for key: " + keys[0].urlsafe())
        aspects_iter =  Aspect.all_aspects_by_tag(keys[0]).iter()
        # dirty, but we need to get the entire list of aspects
        aspects = [x for x in aspects_iter]
        
        # the list of aspects we're going to remove -- in a separate list because loops break if you
        # remove something from the indexin list while the loop is running. duh. 
        aspects_remove = []
        
        
        # loop through them
        for aspect in aspects:
            logging.debug('aspect is ' + aspect.name)
            # loop through our keys and throw the aspect out if it's not there
            #this checks to see if All of the comparisons of key to aspect.tags are successful
            if ( not all( x in aspect.tags for x in keys) ):
                
                # remove the aspect
                aspects_remove.append(aspect)
        
        # now that we know which ones don't match, remove them
        for aspect in aspects_remove:
            aspects.remove(aspect)
        # now that my logic is done is done...
            
          
        ## Serialization section
        # make the output object
        aspect_msgs = AspectMsgs()
        for aspect in aspects:
            aspect_msg = AspectMsg()
            # assign name
            aspect_msg.name = aspect.name
            # assign score
            aspect_msg.score = aspect.score
            # assign key
            aspect_msg.key = aspect.key.urlsafe()
            # more complex because of the need to create all the tag objects
            for tag in aspect.tags:
                aspect_msg.tags.append(tag.urlsafe())
            # add whether the aspect is liked - only if the user is logged in
            if ( user ):
                aspect_msg.liked = aspect.is_liked(user_id=user.user_id())
            
            # add the tag_msgs to the message
            aspect_msgs.msg.append(aspect_msg)


        # set the data to our messages so json returns
        self.context['data'] = aspect_msgs



    """Normal controllers """

    # list = scaffold.list
    """The list methods because of the size of data are restricted to admin """
    @add_authorizations(aspector_auth.require_admin)
    def list(self):
        
        return scaffold.list(self)
        
    """Default view method """
    def view(self, key):
        self.scaffold.display_properties = ("name", "tags")
        return scaffold.view(self, key)
       
    
    

        
    """The edit function, notably customized to use our aspect form """
    # edit = scaffold.edit
    @add_authorizations(aspector_auth.require_admin)
    def edit(self, key):

        # sets the redirect to view instead of normal list
        def set_redirect(controller, container, item):
            controller.scaffold.redirect = controller.uri(action='view', key=item.key.urlsafe())
        
        self.events.scaffold_after_save += set_redirect
        
        self.scaffold.ModelForm = self.AspectEditForm
        return scaffold.edit(self, key)
        
    
    """The add function, notably customized to use our form """
    """The add an aspect form page - note we exclude the score form field
        The previous optional argument tells you what they just got done creating. Should be the urlsafe key of what
        they created.  
    """
    
    @add_authorizations(auth.require_user)
    def add(self):
        logging.getLogger().setLevel(logging.INFO)
        # sets the redirect to view instead of normal list
        def set_redirect(controller, container, item):
            #controller.scaffold.redirect = controller.uri(action='view', key=item.key.urlsafe())
            # now let's redirect them back here but tell them what they just did
            controller.scaffold.redirect = controller.uri(action='add', previous=item.key.urlsafe())
        
        self.events.scaffold_after_save += set_redirect
        # check and see if we got a previous variable
        previous = self.request.params.get('previous');
        if ( previous ):
            aspect = self.util.decode_key(previous).get()
            if ( aspect ):
                self.context['previous'] = aspect
                  
           
        self.scaffold.ModelForm = self.AspectAddForm
        return scaffold.add(self)
    
    
    """The search router - uses search.html template to display search results """
    """The old search function no longer used"""
    #@route
    def search_old(self):
        # get a logger
        logging.getLogger().setLevel(logging.INFO)
        
        # Get the stuff they searched for
        search_terms = cgi.escape(self.request.params.get('search-terms'))
        
        # use the aspect_search method of aspect to get a list of urlsafe keys
        keys_urlsafe = self.meta.Model.aspect_search(search_terms)
        logging.debug('Got these back from aspect_search ' + ''.join(keys_urlsafe) ) 
        
        # Create a new list of keys using the ndb.Key operator
             
        keys = []
        for key_urlsafe in keys_urlsafe:
            keys.append(ndb.Key(urlsafe=key_urlsafe))
        
        aspects = ndb.get_multi(keys)
        
        self.context['aspects'] = aspects
    
    
    """The index router - uses index.html (maybe) template to display index success """
    @route
    # this decoration defines that only admins can access the link, required for cron to work
    @add_authorizations(aspector_auth.require_admin)

    def admin_indexing(self):
        # get a logger
        logging.getLogger().setLevel(logging.INFO)
        # set the view to Json
        self.meta.change_view('json')
        # call both Aspects_index and tag_index and return them
        results = Aspect.aspect_index()
        results += Tag.tag_index()
        self.context['data'] = results
        
    """ Using the new Ferris search api, search. Again, this is a pretty important function.
        The general way this function works is to:
        1) Break everything into OR strings and isolate any individual words that could be searched as tags
        2) Search all of the "AND" items as tags - in this way, AND takes precedence over OR - if a word is next to AND, it'll
        always be in the split list item with the AND, and get operated on as an AND operation
        3) Pass the whole blob of text to the aspect search index as is
        
        ex. the string "kickass magic longbow AND pasta OR character" will get evaluated as:
        -a tag search for aspects that have both "kickass magic longbow" and "pasta" tags --> impossible because tags
        must be one word
        -a tag search for character aspects
        -kickass magic longbow AND pasta OR character passes to the aspect search engine which is very unlikely to return
        any results.
        
        note: there are many parts of this search thing that could be bundled up into helpfer functions
        for clarity.  
    
    """
    @route
    def search(self):
        logging.getLogger().setLevel(logging.INFO)
        # make sure we got a search request- if we didn't, return the index page instead
        if ( not self.request.params.get('search-terms')):
            self.meta.view.template_name = 'index.html'
            return self
        # set the page context search-terms so we can preload the page
        self.context['search_terms'] = self.request.params.get('search-terms') 
        aspect_search_query = "name: " + self.request.params.get('search-terms')
         
        # FIND TAGS
        """ 
        1) do some generic parsing
        2) get a list of the individual search terms "ors"
        3) get a list of the "AND" pairs and search those
        4) bundle it up into a tidy list of aspects
        """
        tag_aspects = [] # this is the list we're going to stuff all the tag queries into 
        # 1) generic cleanup parsing
            # get rid of whitespace
        tag_search_query = re.sub(r" +", " ", self.context['search_terms'] )
            # get rid of trailing/leading spaces
        tag_search_query = re.sub(r"(?:^\s+)|(?:\s+$)", '', tag_search_query)
            # just in case there are any repetitions, remove them, don't want to waste time on that
        tag_search_query = re.sub(r'\b([\w-]+)( \1\b)+', r'\1', tag_search_query)
       
        # 2) get a list of the individual search terms

        # Split the string into "OR" arguments
        tag_or_list = tag_search_query.split(" OR ")
        logging.info("Tag or list: " + str(tag_or_list))

        
        # 2) get a list of all the AND pairs
        tag_and_list = re.findall(r'\b[\w-]+\b\sAND\s\b[\w-]+\b', tag_search_query)
        for tag in tag_and_list:
            logging.info('And group: ' + tag)
        # logging.info("Tag and list " + tag_and_list[0])
        # Now we do a whole bunch of querying for keys from the index.
         
        # master repository for tag keys for the OR list search
        tag_keys = []
        
        # Loop through the or list and search for them all
        
        for tag in tag_or_list:
            # Loop through the entire or list - if they are single words, e.g. possibly tags, query individually
            if ( not ' ' in tag):
                
                tag_query = Tag.query(Tag.name_lower == tag)
                results = [x.key for x in tag_query]
                tag_keys += results
                logging.debug('keys are: ' + str(tag_keys))
            # else if there's an AND in the tag, split it up into AND chunks and search that way
            # this one is a doozy since there's a bunch of querying that goes on!
            elif ( 'AND' in tag):
                tag_and_list = tag.split(" AND ")
                logging.info("Tag And list: " + str(tag_and_list))
                
                
                # create the query to find a list of tags with the given names.  
                tag_query = Tag.query(Tag.name_lower.IN(tag_and_list))
                
                # convert the results into keys
                results = [x.key for x in tag_query]
                # only do any querying if we get at least 2 results
                if ( len(results ) > 1):
                    logging.info("Key list: " + str(results))
                    
                    # now that we have the keys, build our OR statements with yet another loop
                    # create the ndb query!
                    aspect_query = Aspect.query()
                    for key in results:
                        # add a filter to the query!
                        aspect_query = aspect_query.filter(Aspect.tags == key)
                    logging.debug("Aspect query is: " + str(aspect_query))
                    # add what we've got!
                    tag_aspects += aspect_query
        # End the loop through tag_or_list
                    
        
        # loop through the list of tag_keys and add all the aspects that match to our list
        for tag in tag_keys:
            tag_aspects += Aspect.all_aspects_by_tag(tag)
        
        
        # log each one for now
        logging.debug("query is:" + tag_search_query)
        
        # Query for by the aspect text
        self.components.search(
            'auto_ix_Aspect',
            query=aspect_search_query
        )
        
        # add the tag search aspects to our context
        self.context['aspects'] += tag_aspects
        # get rid of any duplicates
        self.context['aspects'] = list(set(self.context['aspects']))
       