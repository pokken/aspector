'''
Created on Jan 14, 2014

@author: rpsands
'''

from ferris import Controller, scaffold, route, add_authorizations, auth, messages, model_form
from google.appengine.ext import ndb
from app.models.tag import Tag
from app.models.aspect import Aspect
from app import auth as aspector_auth
from google.appengine.api.logservice import logging
# import my util class for decoding a list of urlsafe keys into a list of keys
from app import util
import wtforms, cgi

# Message support classes
class TagMsg(messages.Message):
    name = messages.StringField(1)
    score = messages.IntegerField(2)
    key = messages.StringField(3)
    # how many aspects does this tag reference?
    ref = messages.IntegerField(4)


class TagMsgs(messages.Message):
    msg = messages.MessageField(TagMsg, 1, repeated=True)  

class CountMsg(messages.Message):
    count = messages.IntegerField(1)

""" Validation functions for tag name """
def validate_noduplicate(form, field):
    if ( Tag.is_duplicate(field._value() )):
        raise ValueError(field._value() + " already exists in the datastore.")

class Tags(Controller):
    
    class Meta:
        prefixes = ('admin','api',)
        components = (scaffold.Scaffolding, messages.Messaging)



    """ Create the add model form -- note how we specify a query and custom widget for tags in field_args"""
    class TagAddForm(model_form(Tag, exclude=('score',)) ):
        """ just here so we don't have tab errors """
        name = wtforms.StringField("Name", validators=[validate_noduplicate])


    """ 


    """
    # Admin handlers
    admin_list = scaffold.list
    admin_view = scaffold.view        #view a post
    admin_add = scaffold.add          #add a new post
    admin_edit = scaffold.edit        #edit a post
    admin_delete = scaffold.delete    #delete a post

    
    # API handlers
    # api_list = scaffold.list
    
    @add_authorizations(auth.require_user)
    def api_add(self):
        return scaffold.add(self)
    
    """ api_tags is deprecated - do not use  - left for example of how to add util methods like decode_keys"""
    """ More complex one that accepts a list of keys and spits out the json objects
    Accepts a list of keys delimited by & - for now, may change to a post method eventually
    
    @route
    def api_tags(self, keys):
        # get a logger
        logging.getLogger().setLevel(logging.INFO)
        # use the tags_by_keylist method on tags to fetch all of them to this object
        
        # decode the key list
        tag_keys = util.decode_keys(keys)
        
        tags = Tag.tags_by_keylist(tag_keys)

            
        # Let's try creating messages to serialize these into
        tag_msgs = TagMsgs()
        for tag in tags:
            tag_msg = TagMsg()
            tag_msg.name = tag.name
            tag_msg.score = tag.score 
            tag_msgs.msg.append(tag_msg) 
            
        #self.context['data'] = tags
        self.context['data'] = tag_msgs
    """
    
    """ let's try to implement the get by list as a postable json thingyringy """
    """ Accepts a json list of keys as { "keys":[keys] } then searches and responds with json representation
        of these messages 
    """
    @route
    def api_bulk(self):
        # get a logger
        logging.getLogger().setLevel(logging.INFO)

        # reject non-post requests
        if self.request.method != "POST":
            return "No Non-Post requests"
        else:
            # let's parse the response into json
            jsonp = self.util.parse_json(self.request.body)          

            logging.debug(jsonp['keys'])
            
            # get all the key objects from the datastore based on our list of urlsafe keys
            keys = [ndb.Key(urlsafe=key) for key in jsonp['keys']]
            # get all the tag objects back
            tags = Tag.tags_by_keylist(keys)
            # parse the tags into messages
            tag_msgs = TagMsgs() 
            for tag in tags:

                # assign all the properties
                tag_msg = TagMsg()
                tag_msg.name = tag.name
                tag_msg.score = tag.score
                tag_msg.key = tag.key.urlsafe()
                # add the message to our message object
                tag_msgs.msg.append(tag_msg)
            # return the tag_msgs object - much easier to work with!
            self.context['data'] = tag_msgs

            
            
            
            
        # parse the input
    
    # no need to edit tags right now
    # edit = scaffold.edit

    
    """ Custom list controller so that it orders stuff the way we want """
    @add_authorizations(aspector_auth.require_admin)
    def list(self):
        self.context['tags'] = self.meta.Model.all_tags();
    
    """Add a tag form. A few customizations. 
        defines a before_save event trigger that prevents a duplicate from
        being added. 
    """
    @add_authorizations(auth.require_user)
    def add(self):
        # beforesave callback
        # self.context['error_visible'] = 'hidden'
        def before_save_callback(controller, container, item):
            if ( Tag.is_duplicate(item.name)):
                raise ValueError(item.name + " already exists in the Tag library")

        # sets the redirect to view instead of normal list
        def set_redirect(controller, container, item):
            #controller.scaffold.redirect = controller.uri(action='view', key=item.key.urlsafe())
            controller.scaffold.redirect = controller.uri(action='add', previous=item.key.urlsafe())
        
        # check and see if we got a previous variable
        previous = self.request.params.get('previous');
        if ( previous ):
            tag = self.util.decode_key(previous).get()
            if ( tag ):
                self.context['previous'] = tag   
        
        # apply the callback function to the event
        self.events.scaffold_before_save += before_save_callback
        self.events.scaffold_after_save += set_redirect
        
        # assign the form we want to use then return the form
        self.scaffold.ModelForm = self.TagAddForm
        return scaffold.add(self)
    
    """ edit function - rarely used """
    @add_authorizations(aspector_auth.require_admin)
    def edit(self, key):  
        # sets the redirect to view instead of normal list
        def set_redirect(controller, container, item):
            controller.scaffold.redirect = controller.uri(action='view', key=item.key.urlsafe())
        
        self.events.scaffold_after_save += set_redirect
        # assign the form we want to use then return the form
        self.scaffold.ModelForm = self.TagAddForm
            
        return scaffold.edit(self, key)
    
    def view(self, key):
        self.scaffold.display_properties = ("name",)
        return scaffold.view(self, key)
    
    """This is the core controller of the whole danged thing - here we will present a list of tags and let people browse them """
    @route
    def browse(self):
        
        # let's convert this to use messages instead of just crapping out all the tags to the context
        # get them all
        tags = Tag.all_tags()
                    # parse the tags into messages
        tag_msgs = TagMsgs() 
        for tag in tags:
            # start counting the references asynchronously - thank goodness this page only loads once
            count = Aspect.query(Aspect.tags == tag.key ).count_async()
            # assign all the properties
            tag_msg = TagMsg()
            tag_msg.name = tag.name
            tag_msg.score = tag.score
            tag_msg.key = tag.key.urlsafe()
            tag_msg.ref = count.get_result() # gets the result from the count operation we started earlier
            # add the message to our message object
            tag_msgs.msg.append(tag_msg)
        
        #self.context['tags'] = Tag.all_tags()
        # now this uses the message object instead of the datastore query, so we can add the count
        self.context['tags'] = tag_msgs.msg
    
    
    """Unused at the moment - return a count of aspects a given tag is used on - pattern is /api/tags/countref/:key"""
    @route
    def api_countref(self, key):
        logging.getLogger().setLevel(logging.INFO)
        # decode the key       
        tag_key = self.util.decode_key(key)
        # build the count future object just testing asynchronous stuff
        count = Aspect.query(Aspect.tags == tag_key ).count_async()
        
        data = CountMsg()
        data.count = count.get_result()
        
        logging.info(data)
        self.context['data'] = data 
        