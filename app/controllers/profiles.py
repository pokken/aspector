'''
Created on Jan 22, 2014

@author: rpsands
'''
from ferris import Controller, scaffold, route, add_authorizations, auth, messages, model_form
from google.appengine.ext import ndb
from app.models.tag import Tag
from app.models.aspect import Aspect
from app.models.profile import Profile
from app.models.report import Report
from google.appengine.api import users
from google.appengine.api.logservice import logging
# import config
from app.settings import Config

class Profiles(Controller):
    class Meta:
        prefixes = ('admin', 'api',)
        components = (scaffold.Scaffolding, messages.Messaging, )
    
    # Admin handlers
    admin_list = scaffold.list
    admin_view = scaffold.view        #view a post
    #admin_add = scaffold.add          #add a new post
    admin_edit = scaffold.edit        #edit a post
    admin_delete = scaffold.delete    #delete a post
    
    
    """User add function - browsing here will add your user if you don't already exist """
    @add_authorizations(auth.require_user)
    @route
    def add(self):
        
        # create a users object
        user = users.get_current_user()
        
        # Add the user's profile
        Profile.addUser(Profile(), user=user)
        # at the end we need to route them somewhere else - for now we can redirect to
        # root, but we might want to redirect to wherever they came from at some point
        return self.redirect('/')
    
    """ View - this is the stub of the profile viewer - may have to get a different template page eventally
    Note we have to specify the route since it's different than the stock one with the routewith key
    This one only ever views the current user!
    """
    @add_authorizations(auth.require_user)
    @route
    def view(self):
        
       
        user = users.get_current_user()
    
            
        # get the user's profile if it exists--getuser will otherwise create it.
        profile = Profile.getUser(Profile(), user=user)
        
        # set some additional context
            # aspects you like
        likes = Aspect.get_likes(user.user_id())
        self.context['likes'] = likes
            # aspects you have reported...maybe later.
            
        
        return scaffold.view(self, profile.key.urlsafe() )
    
    """config temp route"""
    @route
    def config(self):
        self.meta.change_view('json')
        
        self.context['data'] = Config.browseconfig
        
    """ Hooray, you created something! This is not in use yet but I may use it at some point. """
    @add_authorizations(auth.require_user)
    @route
    def created(self, key):
        key = ndb.Key(urlsafe=key);
        item = key.get();
        # make note of the thing they created
        self.context['item'] = item;