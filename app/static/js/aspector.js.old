// All of the custom javascript for my project!
// Only functions should go here; document ready page specific stuff goes on that page to minimize downloads.

// Globals

// api urls
var aspect_tag_api_url = "/api/aspects/tag/:";
var tag_bulk_api_url = "/api/tags/bulk";
var aspect_edit_url = "/aspects/:*/edit"
// important ids
var aspect_render_div = "#datadiv";
var progress_bar_div = "#progressbar";
var aspect_panel_div = "#aspectpanel-";
var tag_panel_div = "#tag-"

// html to display when waiting for stuff to load
var loading_html = '<div class="progress progress-striped"> \
	  				<div class="progress-bar" role="progressbar" id="aspects_progress" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 5%"> \
						<span class="sr-only">Loading aspects</span> \
					</div> \
				</div';
// html to display if no data comes back from a json request
var no_data_html = '<div class="col-md-12">No aspects found</div>';


// set the url of the we're going to query 

// Gets the json list of aspects corresponding to a given tag key
// Then renders them in the page - probably need to offload the actual
// rendering to another function once we figure out how we want it to look
function renderAspects(tag_key) {
	json_url = aspect_tag_api_url + tag_key;
	//alert(json_url);

	// Clear the progress bar
		$(progress_bar_div).empty();

    // Then clear out the data list before we get started to avoid a double click
    // screwup scenario
    	$(aspect_render_div).empty();

	
	$.getJSON(json_url, function( data ) {

    	if (data.msg != null) {
	    		// Only render the progressbar if there's data
	    		// load the progress bar html
	        	$(progress_bar_div).append($('<p>', {
	        		html: loading_html
	        	}));
	        	// render the aspect list
    			renderAspectsList(data);
    		}
    	else {
    		// If there's no data, let's tell someone
    		$(aspect_render_div).append($('<p>', {
    			html: no_data_html
    		}));
    	}
   
		
	});
}

// This function renders the list to the page once we get our Json data back. This function is stupid complicated and
// you need to make sure it doesn't ever fall apart - the items.push section is the part where all the magic happens.

function renderAspectsList(data) {
	// variable to hold the output html
	var items = [];
	// get the number of objects we're dealing with
	num_items = data.msg.length;
	

	

	$.each(data.msg, function(index, element) {
		// A bunch of hardcoded html here we are gonna move out to variables probably
		// note how there're a number of single quotes hardcoded with "'" - watch out for that
		
		// items.push('<li class="list-group-item col-md-3 panel-default panel" style="height:100%;"><span class="badge">' + element.score + "</span>&nbsp;" + element.name + '</li>')
		
		// the part where we specify an onclick handler here should probably be converted into a
		// jquery statement to add a click handler, it'd make it a lot cleaner
		
		// we're going to add a row tag every third one
		
		// first, if it's the last item never add a new row
		
		
	
/* old function 
		items.push('<div class="col-md-4 col-xs-12"><div class="panel panel-default" id="aspectpanel-'
				+ index + '"><div class="panel-heading aspect-panel"><a href="javascript:void(0)" onClick="renderTags(' + "'"
				+ element.key + "'" + ", "
				+ index + ", this" + ')" style="display:block;">'
				+ element.name + '<b class="caret pull-right"></b></a><input type="hidden" name="'
				+ 'aspect-' + index + '" value=' + "'"
				+ keysToJson(element.tags, element.name) + "'" + ' /></div></div>')
*/
		items.push('<div class="col-md-4 col-xs-12"><div class="panel panel-default" id="aspectpanel-'
				+ index + '"><div class="panel-heading aspect-panel"><div class="row"> \
				<div class="col-xs-10"><a href="javascript:void(0)" onClick="renderTags(' + "'"
				+ element.key + "'" + ", "
				+ index + ", this" + ')" style="display:block;">'
				+ element.name + '<b class="glyphicon glyphicon-folder-open pull-right"></b></a><input type="hidden" name="'
				+ 'aspect-' + index + '" value=' + "'"
				+ keysToJson(element.tags, element.name) + "'" + ' /> \
				</div><div class="col-xs-2"><a href="/reports/:' + element.key + '/report" class="reportLink"> \
				<span class="glyphicon glyphicon-exclamation-sign" title="Report">&nbsp;</span> \
				</div></div></div></div>')
		
		// How close are we to done?
		percent_complete = ( ( index + 1 ) / num_items ) * 100 ;
		// animate the progress bar for each item just in case
		setProgressBar(percent_complete);
		// need to store element.tags somewhere
		
	});

	
	
	// clear out the HTML again just in case
	$(aspect_render_div).empty();
	// then append the html
	$(aspect_render_div).append(items);
	
	// Use the Jquery direct children selector to slice every three off
	// and wrap them in a row
	divs = $("#datadiv > div")
	// do the slice and dice to put rows in every 3
	for ( i = 0; i < divs.length; i+=3 ) {
		divs.slice(i, i+3).wrapAll("<div class='row'></div>");
		
	}
	
	
}

// Sets the progress bar to a particular percent

function setProgressBar(percent) {
	
	// set the css width of the progress bar
	//$('#aspects_progress').css('width', percent + '%')
	// set the aria-valuenow in case that's needed for some reason
	$('#aspects_progress').attr('aria-valuenow', percent)
	// animate the bar to the percent specified
	$('#aspects_progress').animate({width: percent + '%'}, 0);

	
}

// Renders tags when an aspect is clicked - expects the key of the aspect we're working with
// This function is a bit different than renderAspects since we're adding a panel body and  
// disabling the link so it can't be spammed. Lots of stuff to do.
// accepts the panel number too so we know which one we have to modify

function renderTags(key, num_panel, element) {


	// remove the hover on the div above the link
	// $(element).parent().removeClass('aspect-panel');
	// remove the caret
	//$(element).parent().find('.caret').remove();
	// instead of removing the caret, change it into a badge and set some text
	$(element).children('.glyphicon-folder-open').removeClass("glyphicon-folder-open").addClass("glyphicon glyphicon-edit");
	// remove its onclick handler
	$(element).prop("onclick", null);
	// disable the link for the duration of this function

	// set the href to its edit page
	edit_url = aspect_edit_url;
	edit_url = edit_url.replace('*', key);
	// use this change of the onclick handler instead of a link because 
	// of the goofy behavior of settin the href in an onclick method
	$(element).click(function(e) {
		e.preventDefault();
		// location.href = edit_url;
		window.open(edit_url);
	});
	
	
	
	// Render the container element with the appropriate id
	render_tag_html = '<div class="panel-body" id="' + "tag-" + num_panel +'" ><div class="row"></div></div>'
	$(aspect_panel_div + num_panel ).append($(render_tag_html));
	
	// now let's do that json
	
	// first, let's get the json from the hidden input
	json_input_name = 'aspect-' + num_panel;
	json_input_selector = 'input:hidden[name=' + json_input_name + ']'
	json = $(json_input_selector).val();
	
	// now let's post the json
	json_url = tag_bulk_api_url
	
	$.post( json_url, json, function(data) {
		
		// Now that we have our data, do some stuff with it
		$.each(data.msg, function(index, element) {
			// we render tag div with the name, key, and the number of the panel to do it in
			renderTagsDiv(element.name, element.key, num_panel)
		});
		
	}, 'json');
	
	
}

// does all the html rendering

function renderTagsDiv(name, key, num) {
	// build the html
	// note it links to the edit form but opens in a new window
	tag_html = '<div class="col-md-6 col-sm-6 col-xs-12"><a class="' + name + 'link" href="javascript:void(0)">' + name + '</div>'

	
	// append the html
	$(tag_panel_div + num ).find('.row').append(tag_html)
	
	// add an onclick handler that will click the right links - go back to all and click the item they clicked too
	link = '.' + name + 'link';
	// note that we have to unbind the click event to prevent it from triggerin multiple times
	// the reason for this is that this function will enable this click once for every time renderTagsDiv is called
	$(link).unbind('click').click(function(e) {
			$('.all').click();
			$('#' + name).click();
			// debugging
			//alert('Clicking: ' + '#' + name)
	});
	
}


// Simple function to convert a list of keys to json string
function keysToJson(arr, name) {
	// start of the string
	json_str_open = '{ "keys": ['
	json_str_close = '] }'
	json_str_separator = ','
	json_str = json_str_open
	// loop through the objects we got and build a json representation
	$.each(arr, function(index, element) {
		json_itm = '"' + element + '"'
		json_str = json_str + json_itm
		// only add the separator if we're not on the last item
		if ( (index+1) < arr.length) {
			json_str = json_str + json_str_separator
		}
	});

	// finish it off
	json_str = json_str + json_str_close
	return json_str
}