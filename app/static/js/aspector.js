'use strict';
/*
1/21/2014 - rsands
New aspector js in angular to cut down on all the manual html injection. 

*/

/* Globals */ 

//api urls * = first arg, $ = second arg
var aspect_tag_api_url = "/api/aspects/tags";
var tag_bulk_api_url = "/api/tags/bulk";
var aspect_edit_url = "/aspects/:*/edit";
var like_api_url = "/reports/:*/like";

/* Controllers */

// create the app module

var aspectorApp = angular.module('aspectorApp', ['ui.bootstrap']);




/* Configure the interpolator tags */

aspectorApp.config(function($interpolateProvider) {
	  $interpolateProvider.startSymbol('<%');
	  $interpolateProvider.endSymbol('>');
	});

//service for changing an array of keys into a json array of keys

aspectorApp.factory('keysToJson', function() {
	var json_str
		return function(arr) {
			// start of the string
			var json_str_open = '{ "keys": [';
			var json_str_close = '] }';
			var json_str_separator = ',';
			json_str = json_str_open;
			
			// loop through the objects we got and build a json representation
			$.each(arr, function(index, element) {
				var json_itm = '"' + element + '"';
				json_str = json_str + json_itm;
				// only add the separator if we're not on the last item
				if ( (index+1) < arr.length) {
					json_str = json_str + json_str_separator;
				}
			});
			// finish it off
			json_str = json_str + json_str_close;
			return json_str;
		}

});

// Tag post service; supplies a function that can take post data and create a tag
aspectorApp.service('TagPostSvc', function($http) {
	
	this.url = '/api/tags/add'; // the url of the tag api
	this.data = {};
	
	this.post = function(tag_name) {
		 
		this.data.name = tag_name; 
		// console.log("Data is: " + angular.toJson(this.data) );
		
		$http({method: 'POST', url: this.url, data: this.data}).
		success( function (data, status, headers, config) {
			// success function
			// do some stuff, maybe return some data?
			console.log("Got data from tagPostSvc: " + angular.toJson(data));
			//window.location.href = location.href + '?name=Enchiladas%20are%20awesome'; // no need to return anything since
			location.reload();
			
		}).
		error( function (data, status, headers, config) {
			// failure function - return the data
			return data;
		});
	};
});

// key conversion service - used to make sure the underscore in a key is always a hyphen on the way out
// not used anymore now that I clued into how stuff works
aspectorApp.service('keyConvert', function() {
	
	this.convert = function(key) {
		key = key.replace('_', '-');
		return key;
	};
});

// service for storing user information
aspectorApp.service('userSvc', function() {
	// assign constructors
	this.logged_in = false;
	this.nickname = '';
	
	// user function -- if supplied user is length 0, set logged in to false and nickname blank
	this.setUser = function(nickname) {
		if ( nickname.length == 0) {
			this.logged_in = false;
			this.nickname = nickname
			return;
		} else {
			this.logged_in = true;
			this.nickname = nickname;
		}
	};
	
});

// Service for handling liking and not liking stuff. 

aspectorApp.service('likeSvc', function($http) {
	// The array of liked stuff
	this.liked = {};
	// function to check liked status
	this.is_liked = function(key, initial) {
		// if the liked status is not set (null) then try passing the initial value
		if ( this.liked[key] == null ) {
			// note using jsonparse to convert the string literal we get passed it's a string
			if ( typeof(initial) == "string") {
				this.liked[key] = JSON.parse(initial.toLowerCase());
			} else {
				this.liked[key] = initial;
			}
			//console.log("setting initial value to " + initial + " for key:" + key + "array is now: " + angular.toJson(this.liked))
			
		}
		// check key status
		if ( this.liked[key] == true ){
			//console.log("evaluated this.is_liked to true for: key: " + key + "array:" + angular.toJson(this.liked));
			return true;
		} else {
			//console.log("evaluated this.is_liked to false for: key: " + key + "array:" + angular.toJson(this.liked));
			return false;
		}
		
	};
	// function to do the liking 
	this.like = function(key, user) {
		
		// toggle the liked setting
		// if it's true already, set it to false
		if ( this.liked[key] == true ) {
			this.liked[key] = false;
			//alert("setting key to false: " + angular.toJson(this.liked));
		} else {
			//alert("setting key to true");
			this.liked[key] = true;
		}
		
		
		
		// build the api_url
		var api_url = like_api_url;
		// replace the first arg with the key
		api_url = api_url.replace('*', key);
		// replace second arg with the user
		//api_url = api_url.replace('$', user);
		//alert(api_url);
		
		$http({method: 'GET', url: api_url}).
		success( function (data, status, headers, config) {
			// success function
			
			
		}).
		error( function (data, status, headers, config) {
			// failure function
			console.log(status);
		});
		
	}; // end liking function
	

	
});

// Note how we inject the $http dependency into the controller
// along with the keysToJson service

aspectorApp.controller('TagBrowseCtrl', function($scope, $http, keysToJson, likeSvc, userSvc) {
	
	//$scope.keys = ""; // the keys we are going to be searching for
	// $scope.aspects = []; // the repository of aspects to work with
	
	// These two are key to which tags are requested of the api/aspects/tags webservice
	$scope.tags = []; // stores a list of tags that are currently active.
	$scope.tags_active = {}; // stores whether a given tag is active or not, e.g. clicked
	
	$scope.aspects_arr = {}; // array to hold the tags-> aspect array
	
	$scope.aspects_active = []; // which aspects have been clicked?
	$scope.message = ""; // message to display.
	
	$scope.loading_active = false; // store whether we are currently loading json, default to false
	
	$scope.user = userSvc// assign user to the user service. 
	
	// General checkbox watching function - if they go on, do something, if they go off don't poll them
	// when querying for aspects 

	$scope.$watch("tags_active", function(newTags, oldTags, $scope) {
		// clear out the tags array
		$scope.tags = []
		$scope.loading_active = true;
		// for each property on the tag list - where propertynames correspond roughly to tags
		for ( name in newTags ){
			// if the tag is true
			if ( newTags[name] ) {
				// assign the key to the converted tag
				$scope.key = name;
				// assign the key to the list of tags we're goin to be sending to the aspect/tags api
				$scope.tags.push($scope.key);
			};
		}
		//console.log(angular.toJson($scope.tags));
		//console.log(angular.toJson(newTags));
		// execute getAspects function which fetches all the aspects hoohah. only if there are some in there.
		if ( $scope.tags.length > 0) {
			//console.log("Calling getAspects for " + angular.toJson($scope.tags));
			$scope.getAspects($scope.tags);
		} else {
			// if there are no tags, call a reset
			$scope.reset();
		}
		
		// Now that we have our list of tags to fetch in $scope
		
	}, true);
	//
	$scope.log = function(str) {
		
	};
	
	
	
	// Function and array for tracking which things are in the process of loading
	
	$scope.loading = function(key) {
		return $scope.loading_active;
	};
	
	// The rows of our bootstrap grid
	$scope.rows = [];
	var gridColumns;
	var gridRows;
	
	
	// Function to check and see if an aspect is active
	$scope.aspect_active = function(key) {
		if ( $scope.aspects_active.indexOf(key) >= 0 ) {
			//alert ("active called returns true, looking for " + key + " in " + angular.toJson($scope.aspects_active))
			
			return true;
		} else {
			//alert ("active called returns false, looking for " + key + " in " + angular.toJson($scope.aspects_active))
			
			return false;
		}
			
	
	}
	
	// reset the form and blank out the message;
	$scope.reset = function() {
		$scope.reset_rows();
		$scope.message = "";
		// any time a reset occurs, blank out loading
		$scope.loading_active = false;
		// now reset all the tags
		
		for ( name in $scope.tags_active)  {
			$scope.tags_active[name] = false;
		};
		

		
	}
	// separate function to reset rows in case that's all we need to do.
	$scope.reset_rows = function() {
		$scope.rows = [];
	}
	
	// Function to fetch the json
	$scope.getAspects = function(keys) {
		
		//
		console.log(keys);
		
		
		// put the keys in a local json variable
		var json_keys = keysToJson(keys);
		//console.log("getting aspects for" + $scope.keys);

		/* not using this atm
		
		// say we're loading before we get started
		$scope.loading_active[key] = true;
		*/
		
		$scope.result = "This function got triggered for " + keys;
		// Build the url from the scope key and the url const
		var api_url = aspect_tag_api_url;
		
		// do some http stuff
		$http({method: 'POST', url: api_url, data: json_keys}).
			success( function (data, status, headers, config) {
				// success function
				//$scope.aspects = data.msg;
				// If there's no data let's get the heck out of here
				if ( data.msg == null ) {
					// $scope.reset(); // reset it if we don't find any.
					// only reset the rows
					$scope.reset_rows()
					// if we get no data, display a message
					$scope.message = "No aspects found for tag.";

					
					// also clear out loading
					$scope.loading_active = false;
					return;
				} else {
					// reset the scope message if we've got data dude
					$scope.message = "";
				}
				
				
				// let's push the data to rows -- loop into a 2d array
				$scope.rows = [];
				gridColumns = 3; // we want 3 columns
				// if we have 3 columns, we need to figure out how many rows 
				gridRows = (data.msg.length/gridColumns);
				// need to count the aspects separately since we're referencing data.msg
				var datamsgcount = 0
				for ( var i = 0; i < gridRows ; i++) {
					$scope.rows.push([]); // push an array of columns into the first row
					for ( var j = 0; j < gridColumns; j++) {
						if ( data.msg[datamsgcount] != null ) {
							$scope.rows[i][j] = data.msg[datamsgcount];
							datamsgcount++; // increment counter
							// alert(data.msg[j].name + "aspect #" + j);
						}
					}
				}
				// when we're done, turn loading off
				//
				$scope.loading_active = false;
				
			}).
			error( function (data, status, headers, config) {
				// stop loading if it fails too
				// when we're done, turn loading off
				$scope.loading_active = {};
				// failure function
				console.log(status + " \n\r " + angular.toJson(data));
		});
		
	};
	
	// function to get the tags data
	$scope.getTags = function(keys, aspect_key) {
		
		// Build the url from the scope key and the url constant
		var api_url = tag_bulk_api_url;

		
		//push the aspect that just got clicked into the active aspects list 
		if ( $scope.aspects_active.indexOf(aspect_key) == -1) {
			$scope.aspects_active.push(aspect_key);
			//alert("Trying to push " + aspect_key + " into aspects_active")
		} else {
			// if it's already active, go to the aspect edit page, but only if the user is logged in
			if ( $scope.user.logged_in ) {
				window.open("/aspects/:" + aspect_key + "/edit");
			};
			return;
		};
		
		// Now that we've either set the aspect active or kicked it, let's display a loading glyph.
		$scope.loading_active = true;
		
		
		// Try to convert the thing using the keysToJson service! yay!
		keys = keysToJson(keys);
		// do some http stuff
		$http({method: 'POST', url: api_url, data: keys}).
			success( function (data, status, headers, config) {
				// success function
				// #alert(angular.toJson(aspect_key, true))
				$scope.aspects_arr[aspect_key] = data.msg;
				//alert(angular.toJson($scope.aspects_arr, true));
				// turn loading off:
				$scope.loading_active = false;
				
			}).
			error( function (data, status, headers, config) {
				// failure function
				// turn loading off
				$scope.loading_active = false;
				console.log(status);
		});		
		
		
	};
	
	// using the likeSvc
	$scope.liked = likeSvc.liked
	$scope.is_liked = likeSvc.is_liked
	$scope.like = likeSvc.like
	
});

//  Note this is duplicated in tagctrl above so it should probably be a service

// Like controller - for posting likes to the appropriate webservice
aspectorApp.controller('LikeCtrl', function($scope, $http, likeSvc) {
	// using the likeSvc
	$scope.liked = likeSvc.liked
	$scope.is_liked = likeSvc.is_liked
	$scope.like = likeSvc.like
});

// Aspect tagging controller - mostly for sorting and fiddling with aspects on the submit/tag aspect page.

aspectorApp.controller('AspectTagController', function($scope, TagPostSvc) {

	
	$scope.tags = {}; // tags object
	$scope.tag_names = []; // array of tag names
	$scope.tagSearch_default = '[Search all tags]';
	$scope.tagSearch = $scope.tagSearch_default //; current tag search term
	$scope.tagSearchText = ''; // this is what gets displayed if someone types something that isn't found
	// $scope.tagsearch = ''; // tag search textinput
	$scope.tag = '';
	$scope.tagPostSvc = TagPostSvc;
	// turn $scope.tags into an array of names for lookahead
	$scope.parseTagNames = function() {
		$scope.tag_names = []
		for ( var tag in $scope.tags) {
			$scope.tag_names.push(tag);
		};
	};
	
	
	// just testing
	$scope.test = function() {
		console.log("Tag search is: " + $scope.tagsearch);
		console.log("Tag obj is: " + angular.toJson($scope.tags))
		$scope.parseTagNames();
		for ( var tag in $scope.tags ) {
				console.log("Tag: " + tag)
		};
	};
	
	// is the current tag in the box the one we're searching for?
	$scope.tagSearched = function(index) {


		
		if ( $scope.tag_names[index] == $scope.tagSearch ) {
			//console.log("tagSearched true");
			return true;
			
		} else if ( ( $scope.tagSearch.length == 0 ) || ( $scope.tagSearch == $scope.tagSearch_default) ) {
			// if the search field is empty or contains default text
			//console.log("tagSearched true");
			return true;
		} else {
			// other
			//console.log("tagSearched false");
			return false;
		}
	};
	// tag-search-selected callback for the search box
	$scope.tagSearchSelect = function(tag) {
		console.log("user selected:" + tag)
		// set the thing they searched for to true
		$scope.tags[tag] = true;
		// then clear out the box
		$scope.tagSearch = '';
		
	};
	// Determine if the user has entered something in the box that does not match. 
	$scope.tagSearchNotFound = function() {
		// if the current tag search entry is not in our pool of tags, return true, no tag is currently found. 
		// also requires that it not be the default.
		if (  ($scope.tag_names.indexOf($scope.tagSearch) == -1 ) 
				&& ( $scope.tagSearch != $scope.tagSearch_default ) 
				&& ( $scope.tagSearch.length > 2 ) ) {
			$scope.tagSearchText = $scope.tagSearch + " not found. "
			return true;
		};
		
	};
	
	// link tag post svc to our createTag function
	$scope.createTag = function(tag_name) {
		// temporarily we're not going to use TagPostSvc - until we do some major overhauls to the page. 
		location.href = "http://" + location.host + "/tags/add?name=" + tag_name;
	};
	
});


