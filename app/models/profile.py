'''
Created on Jan 22, 2014

@author: rpsands
'''
from ferris.core.ndb import BasicModel
from google.appengine.ext import ndb
from google.appengine.api import users
from google.appengine.api.logservice import logging



"""Basic userprofile but note that you MUST specify an ID at creation"""
class Profile(BasicModel):
    """All we're storing right now is a key and nickname"""
    name = ndb.StringProperty()
    admin = ndb.BooleanProperty(default=False)
    
    

    """Add a user"""
    def addUser(self, user=None):
        # works with the user we pass
        
        profile = Profile(id=user.user_id(), name=user.nickname())
        # only make the user if it doesn't exist'
        if ( not profile.key.get() ):
            profile.put()
    
    """ get a user from a user object """
    def getUser(self, user=None):
        
        # create an object
        profile = Profile(id=user.user_id(), name=user.nickname())
        # if the user doesn't already exist, create it
        profile_fetch = profile.key.get()
        # if we can't get it from the db, put it
        if ( not profile_fetch ):
            profile.put()
            return profile
 
        # if we don't return it after putting it, do it now
        return profile