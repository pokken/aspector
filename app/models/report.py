'''
Created on Jan 20, 2014

@author: rpsands
@note This class is used to handle reporting of aspects or tags, or really anything in our app
'''

from ferris.core.ndb import BasicModel
from google.appengine.ext import ndb
from app.models.tag import Tag
from app.models.aspect import Aspect
from google.appengine.api import users
from google.appengine.api import search
from google.appengine.api.logservice import logging



    
class Report(BasicModel):
    """ Models an individual report entity  - typically a report just has a text and the key it's referencing"""
    class Meta:
        """ maybe some stuff here"""
        
  
    # inherited property created_by is the author
    # inherited property created is the timestamp
    description = ndb.StringProperty(required=True)
    # no specific kind, can report anything!
    reported = ndb.KeyProperty()
    # skipping the lower property for now. 
    