'''
Created on Jan 13, 2014

@author: rpsands
'''

from ferris.core.ndb import BasicModel
from google.appengine.ext import ndb
from app.models.tag import Tag
from app.models.profile import Profile
from google.appengine.api import users
from google.appengine.api import search
from google.appengine.api.logservice import logging
from ferris.behaviors import searchable


"""Function to validate that names only contain a single word """
def nameValidator(self, value):
    # decode string
    name = value.decode("utf-8")
    if ( len(name) > 255):
        raise ValueError('Aspects cannot be longer than 255 characters.')
    else:
        return value

    
class Aspect(BasicModel):
    """ Models an individual aspect entry """
    class Meta:
        behaviors = (searchable.Searchable,)
        index = ('auto_ix_Aspect', 'global',)
        search_exclude = ('score',)
    
    # make the object hashable because I need to remove duplicates occasionally
    def __eq__(self, other):
        return self.key.urlsafe() == other.key.urlsafe()
    
    def __hash__(self):
        return hash(('key', self.key.urlsafe(), 'name', self.name ) )
     
    # static field that configures the name of our aspect index...should probably go to yaml at some point
    index_name = 'auto_ix_Aspect'
    
    # inherited property created_by is the author
    # inherited property created is the timestamp
    name = ndb.StringProperty(validator=nameValidator, required=True)
    score = ndb.IntegerProperty(default=0)
    tags = ndb.KeyProperty(kind=Tag, repeated=True)
    # computed property that'll store all the tag names in lowercase
    name_lower = ndb.ComputedProperty(lambda self: self.name.lower())
    
    likes = ndb.KeyProperty(kind=Profile, repeated=True)
    
    
    @classmethod
    def all_aspects(cls):
        """
        Gets all aspects ordered by date created descending
        """
        return cls.query().order(-cls.created)
    
    @classmethod
    def all_aspects_by_user(cls, user=None):
        """
        Gets all aspects by a given user
        """
        if not user:
            user = users.get_current_user()
        return cls.find_all_by_created_by(user).order(-cls.created)
    
    @classmethod
    def all_aspects_by_tag(cls, key=None):
        # get a logger
        logging.getLogger().setLevel(logging.INFO)
        logging.debug(key.urlsafe() )
        """
        Return all aspects that fit a given tag
        """
        if not key:
            return False;
        else:
            """ query for members of the class containing key """
            # Only query for a given projection
            return cls.query(cls.tags == key)
    
    """ This is used primarily in the search page to fetch all of the tags that an aspect has
        on the browse page we do this by jquery :)
    """       
    def get_tags(self):
        return Tag.tags_by_keylist(self.tags)   
        
    """ This method needs to be refined a lot later since the search algorithm is the key
        We make it pluggable for this reason, just fix this algorithm and it'll get better :)
        Needs to return an ndb.query object 
    """
    @classmethod
    def aspect_search(cls, terms=None):
        logging.getLogger().setLevel(logging.INFO)
     
        # make the index
        try:
            index = search.Index(name=cls.index_name)
        except search.Error:
            logging.exception('Index creation failed')   

        # this can get removed once our cron is working
        # Only re-enable this to test index not updating issues
        #cls.aspect_index()
        
        # search by terms
        try:
            search_terms = "name:" + terms
            results = index.search(search_terms)
            # make a list to store stuff in
            keys = []
            for result in results:
                keys.append(result.doc_id)
                logging.debug('Search found ' + result.doc_id)
            
                
            
        except search.Error:
            logging.exception('Search failed')
        
        return keys
        # term is the thing we're searching for, so we filter...
        #query = cls.query()
        #return query
    
    """ This method populates the full text index and is called from /admin/aspects/indexing  """
    @classmethod
    def aspect_index(cls):
        logging.getLogger().setLevel(logging.INFO)
        # make the index
        try:
            index = search.Index(name=cls.index_name)
        except search.Error:
            logging.exception('Index creation failed')
        
        # list of index results
        index_results = []
        
        # get everything 
        # but only get it for the specified projection
        doc_search_query = cls.query().iter()
        # loop through each one and create a document then stuff it in the index 
        for aspect in doc_search_query:
            # make the document and put it
            doc = search.Document(
                # set the doc_id to the urlsafe key of the aspect
                doc_id = aspect.key.urlsafe(),
                fields=[
                        # make a textfield with the name of the aspect name
                        search.TextField(name='name', value=aspect.name)
                ])
            try:
                index.put(doc)
                # append the doc_id to the list of results
                index_results.append(doc.doc_id)
                logging.debug('Put document' + doc.doc_id)
            except search.Error:
                logging.exception('Failed to put document' + aspect.key.urlsafe())
        
        # return the index results if they're populated, otherwise return failure
        if index_results:
            return index_results
        else:
            return False


    """ check to see if a name already exists. Note that we want to compare lowercase representations
        and then check for plurals. This method could be expanded to check for synonyms later.
    """
    @classmethod
    def is_duplicate(cls, name=None):
        # query for an item, and then 
        # see if we can get a result from the query, if we can, return True, since
        # that means the item is a duplicate. 
        logging.getLogger().setLevel(logging.INFO)
        logging.debug("We're comparing " + name.lower() )
        if ( cls.query(cls.name_lower == name.lower() ).get() ):
            return True
        else: 
            return False
    
    """Answers the question: has someone already liked this object? """
    def is_liked(self, user_id=None):
        # decode
        user_id_str = user_id.decode("utf-8")
        # figure out who the user is
        user_key = ndb.Key( Profile, user_id_str )
        # if the user's key is in our likes, then yes they're liked!
        if ( user_key in self.likes):
            return True
        else:
            return False 
    
    """ Answers the question: Which aspects do I like? Accepts a user id as argument"""
    @classmethod
    def get_likes(cls,  user_id=None):
        # decode
        user_id_str = user_id.decode("utf-8")
        # figure out who the user is
        user_key = ndb.Key( Profile, user_id_str )
        
        return cls.query(cls.likes == user_key)
    
    """Adds a user to the likes if he exists"""
    def toggleLike(self, user_id=None):
        # get a logger
        logging.getLogger().setLevel(logging.INFO)
        if ( not user_id):
            return False
        else:
            # decode the string
            user_id_str = user_id.decode("utf-8")
            # figure out if they already like it
            # make a key
            logging.debug("liking:" + user_id_str)
            
            user_key = ndb.Key( Profile, user_id_str )
            # if the key is in not our current aspect's likes...
            if ( not user_key in self.likes ):
                # add it and save
                self.likes.append(user_key)
                self.put()
                return True
            else:
                # remove the like there is one
                logging.info("removing " + user_key.urlsafe() )
                self.likes.remove(user_key)
                self.put()
                return False
                
                    