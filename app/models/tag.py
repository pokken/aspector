'''
Created on Jan 13, 2014

@author: rpsands
'''
from ferris.core.ndb import BasicModel
from google.appengine.ext import ndb
from google.appengine.api import users
from google.appengine.api import search
from google.appengine.api.logservice import logging
from ferris.behaviors import searchable


"""Function to validate that names only contain a single word """
def nameValidator(self, value):
    #assert ( ' ' not in value)
    # check for spaces
    if ( ' ' in value):
        raise ValueError('Tags must be one word.')
    
    # make sure there are no duplicates
    query = Tag.query(Tag.name_lower == value.lower() )
    if ( query.count(limit=1) > 0 ):
        raise ValueError('Tags must be unique.')
    return value

class Tag(BasicModel):
    # Meta stuff
    class Meta:
        behaviors = (searchable.Searchable,)
        index = ('auto_ix_Tag', 'global')
        exclude = ('score',)
    
    # static field that configures the name of our aspect index...should probably go to yaml at some point
    index_name = 'auto_ix_Tag'

    # inherited property created_by is the author
    # inherited property created is the timestamp
    name = ndb.StringProperty(validator=nameValidator, required=True)
    score = ndb.IntegerProperty(default=0)
    # computed property that'll store all the tag names in lowercase
    name_lower = ndb.ComputedProperty(lambda self: self.name.lower())

    
    @classmethod
    def all_tags(cls):
        """
        Gets all aspects ordered by name alphabetically
        """
        return cls.query().order(cls.name)
    
    @classmethod
    def all_tags_by_user(cls, user=None):
        """
        Gets all aspects by a given user
        """
        if not user:
            user = users.get_current_user()
        return cls.find_all_by_created_by(user).er(-cls.created)
    
    @classmethod
    def tags_by_keylist(cls, keys=None):
        """ Expects a list of keys e.g. from an aspect- seems to behave OK if it's empty
        parse the list of keys into a list of tags! """
        # return cls.query(cls.key == keys)
        # need to do some validation here and make sure we don't ever try to get valid keys
        results = filter(None, ndb.get_multi(keys))
        return results
        
    """ check to see if a name already exists. Note that we want to compare lowercase representations
        and then check for plurals. This method could be expanded to check for synonyms later.
    """
    @classmethod
    def is_duplicate(cls, name=None):
        # query for an item, and then 
        # see if we can get a result from the query, if we can, return True, since
        # that means the item is a duplicate. 
        logging.getLogger().setLevel(logging.INFO)
        logging.debug("is_duplicate comparing " + name.lower() )
        if ( cls.query(cls.name_lower == name.lower() ).get() ):
            return True
        else: 
            return False
        
    
        """ This method populates the full text index and is called from /admin/aspects/indexing  """
    @classmethod
    def tag_index(cls):
        logging.getLogger().setLevel(logging.INFO)
        # make the index
        try:
            index = search.Index(name=cls.index_name)
        except search.Error:
            logging.exception('Index creation failed')
        
        # list of index results
        index_results = []
        
        # get everything 
        # but only get it for the specified projection
        doc_search_query = cls.query().iter()
        # loop through each one and create a document then stuff it in the index 
        for tag in doc_search_query:
            # make the document and put it
            doc = search.Document(
                # set the doc_id to the urlsafe key of the aspect
                doc_id = tag.key.urlsafe(),
                fields=[
                        # make a textfield with the name of the aspect name
                        search.TextField(name='name', value=tag.name)
                ])
            try:
                index.put(doc)
                # append the doc_id to the list of results
                index_results.append(doc.doc_id)
                logging.debug('Put document' + doc.doc_id)
            except search.Error:
                logging.exception('Failed to put document' + tag.key.urlsafe())
        
        # return the index results if they're populated, otherwise return failure
        if index_results:
            return index_results
        else:
            return False
        