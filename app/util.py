'''
Created on Jan 17, 2014
All of my utility classes go here.

@author: rpsands
'''
from google.appengine.ext import ndb

def decode_keys(keys_str=None, kind=None):
    """
    Makes a list of ndb Key objects from the given data
    """
    # strip out the leading colon
    keys_str = keys_str.lstrip(':')
    # split the string into a list of keys
    keys = keys_str.split('&')
    
    # create our output object
    keys_out = []
    
    # loop through and return a list
    for key in keys:
        keys_out.append( ndb.Key(urlsafe=key) )
    
    # Return our list
    return keys_out

"""One word only string property"""
"""Subclass of StringProperty for limiting to one word """
class SingleWordStringProperty(ndb.StringProperty):
    def _validate(self, value):
        # Assert that there are no spaces in the value
        assert (not ' ' in value)
        

        