'''
Created on Jan 16, 2014

@author: rpsands
'''
import wtforms
from app.settings import Config # so we know what tags are key.

class TagsCheckboxWidget(object):
    """
    Widget for MultipleReferenceField. Displays options as checkboxes"""
    def __init__(self, html_tag='div'):
        self.html_tag = html_tag

    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        kwargs['class'] = kwargs.get('class', '').replace('span6', '')
       
        """ Commenting this out since we want it to use my html tag 
        html = [u'<%s %s>'
            % (self.html_tag, wtforms.widgets.html_params(**kwargs))]
        """ 
        # Using multiple HTML attributes; note all the angular specifications in these. might be nice to 
        # make a generic configurable angularcheckboxwidget or something.
            # for the tag categories from Config
        tags_cat_html =[u'<div class="row">\
                            <div class="col-md-3">\
                            <h5>Tag Categories<p class="pull-right"><small>Choose at least one.</small></p></h5>\
                            </div>\
                        </div>\
                        <div class="row">']
        
            # for all the normal tags
        tags_html = [u'<br /><div class="row">\
                          <div class="col-md-8 col-sm-6 col-xs-12">\
                              <input autocomplete="off" ng-focus="tagSearch=\'\'" ng-model="tagSearch"\
                               typeahead="tag for tag in tag_names | filter: $viewValue |  limitTo: 8"\
                               typeahead-on-select="tagSearchSelect(tagSearch)"\
                               class="form-control ngfilter" type="text" id="tag-filter" name="tag-filter">\
                          </div>\
                              <div ng-show="tagSearchNotFound()" class="col-md-4 col-sm-6 col-xs-12">\
                                   <div class="panel"><label><em ng-bind="tagSearchText"></em></label> <a ng-click="createTag(tagSearch)" class="btn btn-info btn-sm">Create It?</a></div>\
                              </div>\
                        </div>\
                  <br />\
                  <div class="row" %s>' % wtforms.widgets.html_params(**kwargs) ]
        # new checkbox rendering, wheee.
        checkbox_name = "tags"
        # loop through the items and 
        # go through all the value/label/selected tuples from the Field and do stuff with them
        for i, item in enumerate(field.iter_choices()):
            value = item[0]
            label = item[1]
            selected = item[2]
            id = "tags-" + str(i)
            # create the angular tag since the percent tag conflicts. yay.
            
            if ( selected ):
                selected = 'ng-checked="true"'
            else:
                selected = 'ng-checked="false"'
            # if the label isn't in config, do something
            if ( not ( label.lower() in Config.browseconfig['tagcategories'] ) ):  
                # append normal tag html
                # add ng-show="tagSearched(%s)" to the div here to make it hide hte tags when searching
                tags_html.append(u'<div ng-show="tagSearched(%s)" class="checkbox col-lg-2 col-md-3 col-sm-4 col-xs-12">\
                                      <input ng-init="tag_names[%s]=\'%s\'" ng-model="tags[\'%s\']" name="%s" type="checkbox" value="%s" id="%s" %s>\
                                      <label class="checkbox" for="%s">%s</label>\
                                  </div>'
                              % (  i, i, label.lower(), label.lower(), checkbox_name, value, id, selected, id, label))
            else:
                # append tag category html
                # Note how we don't use ng-show here since we never want to hide these. 
                tags_cat_html.append(u'<div class="checkbox col-lg-2 col-md-3 col-sm-4 col-xs-12">\
                                      <input ng-init="tag_names[%s]=\'%s\'" ng-model="tags[\'%s\']" name="%s" type="checkbox" value="%s" id="%s" %s>\
                                      <label class="checkbox" for="%s">%s</label>\
                                  </div>'
                              % (  i, label.lower(), label.lower(), checkbox_name, value, id, selected, id, label))
                        
            # close both htmlblocks
        tags_html.append(u'</%s>' % self.html_tag)
        tags_cat_html.append(u'</%s>' % self.html_tag)
        
        return wtforms.widgets.HTMLString(u''.join(tags_cat_html) + u''.join(tags_html))
