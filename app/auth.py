'''
Created on Jan 23, 2014
Contains all authorization methods
@author: rpsands
'''
from google.appengine.ext import ndb
from google.appengine.api import users
from app.models.profile import Profile

import logging


"""authorizations for the user profile subsystem"""
def require_admin(controller):
    # IF there's a user, see if it's an admin
    if (controller.user):
        # get the user id
        user_id = controller.user.user_id()
        # create the user's key
        user_key = ndb.Key( Profile, user_id )
        # get the user's profile if it exists
        profile = user_key.get()
        # if the user actually exists
        if ( profile ):
            

            if ( profile.admin ):
                return True
            else:
                return (False, "User is not an administrator.")
        else:
            # If the user has no profile, bounce it to the profile creation routine then reject it.
            Profile.addUser( Profile(), controller.user )
            profile = user_key.get() 
            return ( False, "User is not an administrator.")